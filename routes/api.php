<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api', 'guest'], 'prefix' => 'v1'], function () {
	Route::post('auth/login/pelaksana', 'ApiController@doLoginPelaksana');
	Route::post('auth/login/pimpinan', 'ApiController@doLoginPimpinan');
	Route::post('reset-password', 'ApiController@doResetPassword');
});

Route::group(['middleware' => ['auth:api'], 'prefix' => 'v1'], function () {
	Route::get('auth/user', 'ApiController@getUserData');

	// Module
	Route::post('add-balance', 'ApiController@addWalletBalance');
	Route::post('direct-trx', 'ApiController@submitDirectTrx');
	Route::post('edit-profile', 'ApiController@doUpdateProfile');
	Route::post('order/material', 'ApiController@doOrderMaterial');
	Route::post('order/jasa', 'ApiController@doOrderJasa');
	Route::post('absent', 'ApiController@postAbsent');
	Route::post('order/acc-order', 'ApiController@doTerimaBarang');
	Route::post('list/barang', 'ApiController@getListBarang');

	Route::get('pegawai', 'ApiController@getPegawai');
	Route::get('list/direct', 'ApiController@getListDirectTrx');
	Route::get('announcement', 'ApiController@getAnnouncement');
	Route::get('announcement/read-all', 'ApiController@doReadNotification');
});