<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB;
use Auth;
use File;
use App\User;
use App\DirectTrx;

class ApiController extends Controller
{
    //
    public function doLoginPelaksana(Request $request) {
        $valid = Validator::make($request->all(), [
            'username' => 'required|string',
            'password' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => false, 'message' => 'Username atau Password Salah'], 200);
        } else {
            $ifEmailisExists = User::where('username', $request->username)->get();
            if (count($ifEmailisExists) <= 0) {
                # code...
                return response(['status' => false, 'message' => 'Username Tidak Terdaftar'], 200);
            } else {
                if ( Auth::attempt(['username' => $request->username, 'password' => $request->password, 'hak_akses' => 'pelaksana']) ) {
                    # code...
                    $user = Auth::user();
                    $project = DB::table('master_project')->where('user_id', Auth::user()->kd_user)->first();
                    $token = $user->createToken('Pelaksana Token')->accessToken;
                    return response(['status' => true, 'message' => 'Login Berhasil', 'data' => $token, 'username' => Auth::user()->username, 'proyek' => $project->project_name], 200);
                } else {
                    return response(['status' => false, 'message' => 'Username atau Password Salah'], 200);
                }
            }
        }
    }

    public function doLoginPimpinan(Request $request) {
        $valid = Validator::make($request->all(), [
            'username' => 'required|string',
            'password' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => false, 'message' => 'Username atau Password Salah'], 200);
        } else {
            $ifEmailisExists = User::where('username', $request->username)->get();
            if (count($ifEmailisExists) <= 0) {
                # code...
                return response(['status' => false, 'message' => 'Username Tidak Terdaftar'], 200);
            } else {
                if ( Auth::attempt(['username' => $request['username'], 'password' => $request['password'], 'hak_akses' => 'pimpinan']) ) {
                    $user = Auth::user();
                    $token = $user->createToken('Pimpinan Token')->accessToken;
                    return response(['status' => true, 'message' => 'Login Berhasil', 'data' => $token, 'username' => Auth::user()->username], 200);
                } else {
                    return response(['status' => false, 'message' => 'Username atau Password Salah'], 200);
                }
            }
        }
    }

    public function doResetPassword(Request $request) {
        $valid = Validator::make($request->all(), [
                'username' => 'required'
            ]);
            
        if ($valid->fails()) {
            # code...
            return response(['status' => false, 'message' => 'Username Masih Kosong'], 200);
        } else {
            $cekUsername = User::where('username', $request->username)->get();
            if (count($cekUsername) <= 0) {
                # code...
                return response(['status' => false, 'message' => 'User Tidak Terdaftar'], 200);
            } else {
                $reset = User::where('username', $request->username)->update(['password' => bcrypt('1234567890')]);
                if ($reset) {
                    # code...
                    return response(['status' => true, 'message' => 'Password Berhasil Direset'], 200);
                } else {
                    return response(['status' => false, 'message' => 'Gagal Mereset Password'], 200);
                }
            }
        }
    }

    public function getUserData(Request $request) {
        $cekUsername = User::where('kd_user', Auth::user()->kd_user)->get();
        if (count($cekUsername) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Username Tidak Terdaftar'], 200);
        } else {
            $user = User::where('kd_user', Auth::user()->kd_user)
                        ->join('master_pegawai as a', 'a.kd_pegawai', 'user_login.kd_pegawai')
                        ->join('master_project as b', 'b.user_id', 'user_login.kd_pegawai')
                        ->first();
            return response(['status' => true, 'message' => 'User Ditemukan', 'data' => $user['username'], 'wallet' => $user['wallet'], 'alamat' => $user['alamat_pegawai'], 'npwp' => $user['npwp_pegawai'], 'fullname' => $user['nama_pegawai']], 200);
        }
    }

    public function addWalletBalance(Request $request) {
        $valid = Validator::make($request->all(), [
            'nominal' => 'required',
            'keterangan' => 'required|string'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => false, 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $add = DB::table('master_project')->where('user_id', Auth::user()->kd_user)->increment('wallet', $request->nominal);
            \DB::table('history_topup')->insert([
                'user_id' => Auth::user()->kd_user,
                'topup_amount' => $request->nominal,
                'keterangan' => $request->keterangan
            ]);

            if ($add) {
                # code...
                return response(['status' => true, 'message' => 'Wallet Berhasil Ditambahkan'], 200);
            } else {
                return response(['status' => false, 'message' => 'Gagal Menambahkan Wallet'], 200);
            }
        }
    }

    public function submitDirectTrx(Request $request) {
        $valid = Validator::make($request->all(), [
            'item_name' => 'required',
            'item_qty' => 'required',
            'item_price' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => false, 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $trx = DirectTrx::create([
                'user_id' => Auth::user()->kd_user,
                'item_name' => $request->item_name,
                'item_qty' => $request->item_qty,
                'item_price' => $request->item_price,
                'item_total' => ((int)$request->item_qty * (int)$request->item_price)
            ]);

            if ($trx) {
                # code...
                User::where('kd_user', Auth::user()->kd_user)->decrement('wallet', ((int)$request->item_qty * (int)$request->item_price));
                return response(['status' => true, 'message' => 'Pembelanjaan Berhasil Dilakukan'], 200);
            } else {
                return response(['status' => false, 'message' => 'Gagal Melakukan Pembelanjaan'], 200);
            }
        }
    }

    public function doUpdateProfile(Request $request) {
        $valid = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required',
            'alamat' => 'required',
            'npwp' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => false, 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $user = \DB::table('master_pegawai')->where('kd_pegawai', Auth::user()->kd_pegawai)->limit(1)->update([
                'nama_pegawai' => $request->name,
                'alamat_pegawai' => $request->alamat,
                'npwp_pegawai' => $request->npwp
            ]);

            return response(['status' => true, 'message' => 'Profile Telah Diupdate'], 200);
        }
    }
    
    public function doOrderMaterial(Request $request) {
        $valid = Validator::make($request->all(), [
            'item_name' => 'required',
            'item_qty' => 'required',
            'date' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => false, 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $create = DB::table('trx_order')->insert([
                'trx_code' => 'TRX-MT'.date('Y').date('m').rand(0000,9999),
                'user_id' => Auth::user()->kd_user,
                'item_name' => $request->item_name,
                'item_qty' => $request->item_qty,
                'type' => 'material',
                'is_confirm' => 'N',
                'date_order' => $request->date
            ]);

            if ($create) {
                # code...
                return response(['status' => true, 'message' => 'Order Berhasil Dilakukan'], 200);
            } else {
                return response(['status' => false, 'message' => 'Gagal Melakukan Order'], 200);
            }
        }
    }

    public function doOrderJasa(Request $request) {
        $valid = Validator::make($request->all(), [
            'item_name' => 'required',
            'item_qty' => 'required',
            'date' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => false, 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $create = DB::table('trx_order')->insert([
                'trx_code' => 'TRX-JS'.date('Y').date('m').rand(0000,9999),
                'user_id' => Auth::user()->kd_user,
                'item_name' => $request->item_name,
                'item_qty' => $request->item_qty,
                'type' => 'jasa',
                'is_confirm' => 'N',
                'date_order' => $request->date
            ]);

            if ($create) {
                # code...
                return response(['status' => true, 'message' => 'Order Berhasil Dilakukan'], 200);
            } else {
                return response(['status' => false, 'message' => 'Gagal Melakukan Order'], 200);
            }
        }
    }
    
    public function getPegawai() {
        $data = DB::table('master_pegawai')->where('jabatan', '!=', 'admin')->where('status', 'aktif')->get();
        if (count($data) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Pegawai Kosong', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Pegawai Ditemukan', 'data' => $data], 200);
        }
    }

    public function postAbsent(Request $request) {
        $valid = Validator::make($request->all(),[
            'geo_lat' => 'required',
            'geo_long' => 'required',
            'images' => 'image|mimes:jpg,jpeg,png|required',
            'signature' => 'image|mimes:jpg,jpeg,png|required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => false, 'message' => 'Form Tidak Lengkap', 'cek' => $request->all()], 200);
        } else {
            $create = DB::table('absent')->insert([
                'user_id' => Auth::user()->kd_user,
                'geo_lat' => $request->geo_lat,
                'geo_long' => $request->geo_long,
                'images' => $this->savePhoto($request->file('images')),
                'signature' => $this->saveSignature($request->file('signature'))
            ]);

            if ($create) {
                # code...
                return response(['status' => true, 'message' => 'Absen Berhasil'], 200);
            } else {
                return response(['status' => false, 'message' => 'Gagal Melakukan Absensi'], 200);
            }
        }
    }

    public function getListDirectTrx() {
        $data = DB::table('trx_direct')->get();
        if (count($data) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Tidak Ada Data', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Data Ditemukan', 'data' => $data], 200);
        }
    }

    public function doTerimaBarang(Request $request) {
        $valid = Validator::make($request->all(), [
            'invoice_no' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => false, 'message' => 'No. Invoice Masih Kosong'], 200);
        } else {
            $cek = DB::table('trx_order')->where('trx_code', $request->invoice_no)->where('is_confirm', 'N')->get();
            if (count($cek) <= 0) {
                # code...
                return response(['status' => false, 'message' => 'Invoice Tidak Ada'], 200);
            } else {
                $post = DB::table('trx_order')->where('trx_code', $request->invoice_no)->update([
                    'is_confirm' => 'Y',
                    'date_confirmed' => date('Y-m-d')
                ]);

                if ($post) {
                    # code...
                    return response(['status' => true, 'message' => 'Barang Berhasil Diterima'], 200);
                } else {
                    return response(['status' => false, 'message' => 'Gagal Menerima Barang'], 200);
                }
            }
        }
    }
    
    public function getAnnouncement() {
        $data = DB::table('announcement')->where('is_read', 'N')->get();
        return response(['status' => true, 'message' => 'List Announcement', 'data' => $data], 200);
    }
    
    public function doReadNotification() {
        $data = DB::table('announcement')->where('is_read', '=', 'N')->update(array('is_read' => 'Y'));
        if ($data) {
            # code...
            return response(['status' => true, 'message' => 'Berhasil Menandai'], 200);
        } else {
            return response(['status' => false, 'message' => 'Gagal Menandai'], 200);
        }
    }

    public function getListBarang(Request $request) {
        $valid = Validator::make($request->all(),[
            'type' => 'required'
        ]);

        if ($valid->fails()) {
            return response(['status' => false, 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            if ($request['type'] == "jasa") {
                $data = DB::table('master_barang')->select('kd_barang', 'nama_barang')->where('jenis_barang', 'jasa')->get();
                return response(['status' => true, 'message' => 'List Barang', 'data' => $data], 200);
            } else {
                $data = DB::table('master_barang')->select('kd_barang', 'nama_barang')->where('jenis_barang', 'material')->get();
                return response(['status' => true, 'message' => 'List Barang', 'data' => $data], 200);
            }
        }
    }

    protected function savePhoto($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'absen';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $create['image'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $create['image'];
    }

    protected function saveSignature($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'ttd';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $create['image'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $create['image'];
    }

    protected function deletePhoto($photo)
    {
        File::delete($photo);
        return $photo;
    }
}
