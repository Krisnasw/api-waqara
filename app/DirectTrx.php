<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DirectTrx extends Model
{
    //
    protected $table = 'trx_direct';
    protected $fillable = ['user_id', 'item_name', 'item_qty', 'item_price', 'item_total', 'status'];

    public $timestamps = false;
}
